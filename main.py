
import paho.mqtt.client as paho
import yaml
import os
import asyncio

from blague import Blague

async def main():
    print("Ouverture du programme..")
    with open(os.path.dirname(__file__) + "/config.yaml", "r") as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    print("Récupération de la blague")
    # Récupération de la blague
    blague = Blague(cfg["blagues-api"]["token"]);

    joke = await blague.getBlague(cfg["blagues-api"]["disallow_categories"]);

    print(joke)

    print("Envoi sur le broker MQTT")
    # Envoi mqtt
    client = paho.Client(cfg["mqtt"]["client"])
    client.username_pw_set(cfg["mqtt"]["user"], cfg["mqtt"]["password"])
    client.connect(cfg["mqtt"]["broker"], cfg["mqtt"]["port"])
    ret = client.publish(cfg["mqtt"]["topic"], joke.joke + "\n" + joke.answer, retain=True)

    print("Fin du programme")

asyncio.run(main())