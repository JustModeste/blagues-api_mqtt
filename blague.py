from blagues_api import BlaguesAPI
import asyncio

#
# Class pour récupérer une blague sur l'API de blagues-api.fr
#
class Blague:
    #
    # Constructeur
    #
    def __init__(self, token):
        self.blagues = BlaguesAPI(token)

    #
    # Récupération d'une blague
    #
    # @param disallow_categories    Liste des catégories à ne pas prendre en compte
    # @return   Une blague de l'API
    # 
    async def getBlague(self, disallow_categories = []):
        return await self.blagues.random(disallow=disallow_categories)