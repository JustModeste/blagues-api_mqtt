# blagues-api_mqtt

Récupère une blague sur le site www.blagues-api.fr et l'envoi sur le broker MQTT

## Getting started
Il vous faut créer un TOKEN sur le site www.blagues-api.fr

Configurer le fichier config.yaml avec vos paramètres

## Installation des dépendances python
```
pip3 install -r requirements.txt
```

## Exécution
```
python3 blague.py
```

## Mon configuration.yaml de Home-Assistant:
```
sensor:
  - platform: mqtt
    name: "blague_du_jour"
    state_topic: "blagues/blague_du_jour"
```

## Ma card Lovelace
```
type: entities
entities:
  - entity: sensor.blague_du_jour
    icon: mdi:emoticon-lol
    name: ' '
title: Blague du jour
```
